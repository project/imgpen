<?php

namespace Drupal\imgpen\Plugin\CKEditorPlugin;

use Drupal\imgpen\Plugin\ImgPenEcosystemCKEditorPlugin;
use Drupal\editor\Entity\Editor;

/**
 * Defines the plugin.
 *
 * @CKEditorPlugin(
 *   id = "ImgPen",
 *   label = @Translation("ImgPen"),
 *   module = "imgpen"
 * )
 */
class imgpen extends ImgPenEcosystemCKEditorPlugin
{

    /**
     * {@inheritdoc}
     */
    public function getButtonsDef()
    {
        return array(
            'ImgPen' => array(
                'label' => 'Edit selected image',
                'image' => 'https://cdn.n1ed.com/cdn/buttons/ImgPen.png'
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getPluginName()
    {
        return "ImgPen";
    }

    /**
     * {@inheritdoc}
     */
    public function getModuleName()
    {
        return "imgpen";
    }

    /**
     * {@inheritdoc}
     */
    public function getDependencies(Editor $editor)
    {
        return array(
            //"FileUploader"
        );
    }

    /**
     * {@inheritdoc}
     */
    public function addControlsToForm(&$form, $editor, $config)
    {
        $this->addBooleanToForm($form, $config, "enableImgPen", true);
    }
}
